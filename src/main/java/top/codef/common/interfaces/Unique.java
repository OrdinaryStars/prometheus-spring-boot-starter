package top.codef.common.interfaces;

public interface Unique {

	/**
	 * 在一群相同组件结构中，谁是那个谁
	 * 
	 * @return
	 */
	public String name();
}
