//package top.codef.codef.components;
//
//import org.springframework.cloud.client.discovery.event.InstancePreRegisteredEvent;
//import org.springframework.cloud.client.serviceregistry.Registration;
//import org.springframework.context.ApplicationListener;
//
//public class ServicePreRegistListener implements ApplicationListener<InstancePreRegisteredEvent> {
//
//	private Registration registration;
//
//	@Override
//	public void onApplicationEvent(InstancePreRegisteredEvent event) {
//		this.registration = event.getRegistration();
//	}
//
//	public Registration getRegistration() {
//		return registration;
//	}
//}
