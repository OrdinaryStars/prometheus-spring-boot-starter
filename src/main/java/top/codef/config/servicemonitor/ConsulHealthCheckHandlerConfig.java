package top.codef.config.servicemonitor;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.consul.discovery.ConditionalOnConsulDiscoveryEnabled;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ecwid.consul.v1.ConsulClient;

import top.codef.config.annos.ConditionalOnServiceMonitor;
import top.codef.microservice.components.ConsulHealthCheckHandler;
import top.codef.microservice.interfaces.HealthCheckHandler;

@Configuration
@ConditionalOnServiceMonitor
@ConditionalOnConsulDiscoveryEnabled
public class ConsulHealthCheckHandlerConfig {

	@Bean
	@ConditionalOnMissingBean
	public HealthCheckHandler healthCheckHandler(ConsulClient consulClient) {
		HealthCheckHandler handler = new ConsulHealthCheckHandler(consulClient);
		return handler;
	}
}
