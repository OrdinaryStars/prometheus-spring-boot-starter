package top.codef.exceptionhandle.interfaces;

import top.codef.pojos.NoticeStatistics;
import top.codef.pojos.PromethuesNotice;

public interface NoticeStatisticsRepository {

	public NoticeStatistics increaseOne(PromethuesNotice exceptionNotice);

	public void increaseShowOne(NoticeStatistics exceptionStatistics);

	public void clear();
}
