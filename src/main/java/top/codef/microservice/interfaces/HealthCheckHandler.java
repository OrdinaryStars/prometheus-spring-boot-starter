package top.codef.microservice.interfaces;

import org.springframework.cloud.client.ServiceInstance;

import top.codef.properties.servicemonitor.ServiceCheck;

@FunctionalInterface
public interface HealthCheckHandler {

	public boolean isHealthy(ServiceInstance serviceInstance, ServiceCheck serviceCheck);

}
