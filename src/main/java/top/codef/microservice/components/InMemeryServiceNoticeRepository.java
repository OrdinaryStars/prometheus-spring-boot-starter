package top.codef.microservice.components;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import top.codef.microservice.interfaces.ServiceNoticeRepository;
import top.codef.pojos.servicemonitor.MicroServiceReport;
import top.codef.pojos.servicemonitor.ServiceHealthProblem;
import top.codef.pojos.servicemonitor.ServiceInstanceLackProblem;

public class InMemeryServiceNoticeRepository implements ServiceNoticeRepository {

	private MicroServiceReport lastReport = null;

	private MicroServiceReport currentReport = new MicroServiceReport();

	@Override
	public void addServiceLackProblem(ServiceInstanceLackProblem serviceInstanceLackProblem) {
		currentReport.putLackInstanceService(serviceInstanceLackProblem.getServiceName(), serviceInstanceLackProblem);
	}

	@Override
	public void addServiceHealthProblem(ServiceHealthProblem serviceHealthProblem) {
		currentReport.putUnHealthyService(serviceHealthProblem.getServiceName(), serviceHealthProblem);
	}

	@Override
	public synchronized MicroServiceReport report() {
		if (currentReport.isNeedReport()) {
			lastReport = currentReport;
			currentReport = new MicroServiceReport();
			return lastReport;
		}
		return currentReport;
	}

	@Override
	public void addLackServices(Set<String> serviceName) {
		currentReport.putLackServices(serviceName);
	}

	@Override
	public void addLackServices(String... serviceName) {
		Set<String> set = new HashSet<String>();
		set.addAll(Arrays.asList(serviceName));
		currentReport.putLackServices(set);
	}

}
