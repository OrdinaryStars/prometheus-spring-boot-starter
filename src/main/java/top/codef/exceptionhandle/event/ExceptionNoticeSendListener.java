package top.codef.exceptionhandle.event;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import top.codef.common.abstracts.AbstractNoticeSendListener;
import top.codef.exceptionhandle.interfaces.NoticeStatisticsRepository;
import top.codef.notice.NoticeComponentFactory;
import top.codef.properties.frequency.NoticeFrequencyStrategy;
import top.codef.text.NoticeTextResolverProvider;

public class ExceptionNoticeSendListener extends AbstractNoticeSendListener {

	private final static Log logger = LogFactory.getLog(ExceptionNoticeSendListener.class);

	/**
	 * @param noticeFrequencyStrategy
	 * @param exceptionNoticeStatisticsRepository
	 * @param resolverProvider
	 * @param noticeComponentFactory
	 * @param resolverName
	 */
	public ExceptionNoticeSendListener(NoticeFrequencyStrategy noticeFrequencyStrategy,
			NoticeStatisticsRepository exceptionNoticeStatisticsRepository, NoticeTextResolverProvider resolverProvider,
			NoticeComponentFactory noticeComponentFactory, String resolverName) {
		super(noticeFrequencyStrategy, exceptionNoticeStatisticsRepository, resolverProvider, noticeComponentFactory);
	}

	@Override
	public void onApplicationEvent(ExceptionNoticeEvent event) {
		logger.debug("消息同步发送");
		send(event.getBlameFor(), event.getExceptionNotice());
	}

}
